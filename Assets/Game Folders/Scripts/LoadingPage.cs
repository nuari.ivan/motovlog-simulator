using GaweDeweStudio;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LoadingPage : Page
{
    [SerializeField] private Slider sliderLoading;
    [SerializeField] private TMP_Text sliderText;

    private float t;
    private bool isLoading = false;

    private void OnEnable()
    {
        t = 0f;
    }

    private void Update()
    {
        if(t <= 1f)
        {
            t += Time.deltaTime / 10f;
            sliderLoading.value = t;
            sliderText.text = $"Loading. .      {Mathf.FloorToInt(t * 100f)} <color=black>%</color>";
        }
        else
        {
            if(!isLoading)
            {
                ChangeScene();
            }
        }
    }

    private void ChangeScene()
    {
        isLoading = true;
        SceneManager.LoadSceneAsync(2, LoadSceneMode.Single);
    }
}
