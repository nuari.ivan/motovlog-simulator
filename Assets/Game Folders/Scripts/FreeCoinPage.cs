using GaweDeweStudio;
using UnityEngine;
using UnityEngine.UI;

public class FreeCoinPage : Page
{
    [SerializeField] private Button yesButton;
    [SerializeField] private Button backButton;

    [SerializeField] private GameObject panelCongrats;
    [SerializeField] private Button closeButton;

    protected override void Start()
    {
        base.Start();
        panelCongrats.SetActive(false);

        yesButton.onClick.AddListener(() =>
        {
            // watch ads here
            GameManager.Instance.GetAds().RequestRewardedAds("Free Coin", (isDone) =>
            {
                if(isDone)
                {
                    GameManager.Instance.AddCoin(1000);
                    yesButton.interactable = false;
                    panelCongrats.SetActive(true);
                }
            });
        });

        backButton.onClick.AddListener(() => GameManager.Instance.ChangeState(GameState.Menu));
        closeButton.onClick.AddListener(() => panelCongrats.SetActive(false));
    }
}
