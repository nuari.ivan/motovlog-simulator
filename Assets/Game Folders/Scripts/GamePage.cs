using GaweDeweStudio;
using System;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GamePage : Page
{
    [SerializeField] private Button pauseButton;
    [SerializeField] private Button derekButton;
    [SerializeField] private Button rideButton;
    [SerializeField] private Button walkButton;
    [SerializeField] private Button jumpButton;
    [SerializeField] private Button changeCamButton;
    [SerializeField] private Button jampingButton;

    [SerializeField] private TMP_Text labelMoneyText;
    [SerializeField] private TMP_Text labelTimerText;

    [SerializeField] private GameObject[] playerPanels;

    [SerializeField] private Joystick _joystick;
    [SerializeField] private Joystick _cameraJoystick;

    private Vector2 startPos;
    private Vector2 direction;

    private bool directionChosen;

    protected override void Start()
    {
        base.Start();
        GameSetting.Instance.OnNearBike += Instance_OnNearBike;
        GameSetting.Instance.OnRiding += Instance_OnRiding;
        GameSetting.Instance.OnWalk += Instance_OnWalk;
        GameSetting.Instance.OnTimerUpdate += Instance_OnTimerUpdate;

        rideButton.gameObject.SetActive(false);
        jumpButton.onClick.AddListener(() => GameSetting.Instance.PlayerJump(true));

        pauseButton.onClick.AddListener(() => GameManager.Instance.ChangeState(GameState.Pause));

        derekButton.onClick.AddListener(() =>
        {
            //reset bike position
            GameSetting.Instance.GetBike().transform.position = new Vector3(489.6714f, 35.822f, 616.0268f);
            GameSetting.Instance.GetBike().ResetBike();
        });
        rideButton.onClick.AddListener(RideOnBike);
        walkButton.onClick.AddListener(RideOnBike);

        changeCamButton.onClick.AddListener(() => GameSetting.Instance.ChangeCam());
    }

    private void Instance_OnTimerUpdate(int playTime)
    {
        string v = playTime < 10 ? $"0{playTime}" : $"{playTime}";
        labelTimerText.text = $"00 : {v}";
    }

    private void OnDestroy()
    {
        GameSetting.Instance.OnNearBike -= Instance_OnNearBike;
        GameSetting.Instance.OnRiding -= Instance_OnRiding;
        GameSetting.Instance.OnWalk -= Instance_OnWalk;
        GameSetting.Instance.OnTimerUpdate -= Instance_OnTimerUpdate;
    }

    private void Instance_OnWalk()
    {
        playerPanels[0].gameObject.SetActive(true);
        playerPanels[1].gameObject.SetActive(false);

        _cameraJoystick.gameObject.SetActive(true);

        RectTransform camJoystickRect = _cameraJoystick.GetComponent<RectTransform>();
        camJoystickRect.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Right, 150f , camJoystickRect.rect.width);
    }

    private void Instance_OnRiding()
    {
        playerPanels[1].gameObject.SetActive(true);
        playerPanels[0].gameObject.SetActive(false);

        _cameraJoystick.gameObject.SetActive(false);

        /*RectTransform camJoystickRect = _cameraJoystick.GetComponent<RectTransform>();
        camJoystickRect.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, 150f, camJoystickRect.rect.width);*/
    }

    private void RideOnBike()
    {
        switch (GameSetting.Instance.GetCurrentState())
        {
            case PlayModeState.OnCharacter:
                GameSetting.Instance.ChangeState(PlayModeState.OnBike);
                break;
            case PlayModeState.OnBike:
                GameSetting.Instance.ChangeState(PlayModeState.OnCharacter);
                break;
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha5))
        {
            switch (GameSetting.Instance.GetCurrentState())
            {
                case PlayModeState.OnCharacter:
                    GameSetting.Instance.ChangeState(PlayModeState.OnBike);
                    break;
                case PlayModeState.OnBike:
                    GameSetting.Instance.ChangeState(PlayModeState.OnCharacter);
                    break;
            }
        }

        if(_joystick != null)
        {
            GameSetting.Instance.SetInput(_joystick.Horizontal , _joystick.Vertical );
        }

        if(_cameraJoystick != null)
        {
            GameSetting.Instance.SetCameraInput(_cameraJoystick.Horizontal, _cameraJoystick.Vertical);
        }
    }

    

    private void Instance_OnNearBike(bool isNear)
    {
        rideButton.gameObject.SetActive(isNear);
    }

    private void OnEnable()
    {
        if(GameManager.Instance == null)
        {
            return;
        }
        labelMoneyText.text = $"Rp. {GameManager.Instance.GetPlayerData().coin} ,00";
    }
}
