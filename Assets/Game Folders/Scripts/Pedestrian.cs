using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Pedestrian : MonoBehaviour
{
    [SerializeField] private TargetPointPedestrianGroup targetPoint;
    [SerializeField] private List<GameObject> allModels = new List<GameObject>();

    private int poin = 0;

    private NavMeshAgent agent;

    private void Start()
    {
        agent = GetComponent<NavMeshAgent>();

        for (int i = 0; i < transform.childCount -1; i++)
        {
            allModels.Add(transform.GetChild(i).gameObject);
        }

        foreach (var item in allModels)
        {
            item.SetActive(false);
        }

        allModels[Random.Range(0, allModels.Count)].SetActive(true);
    }

    private void Update()
    {
        if(targetPoint != null)
        {
            Vector3 _targetPos = targetPoint.GetPointsTarget()[poin].position;
            float _distance = Vector3.Distance(transform.position, _targetPos);
            if(_distance > 0.3f)
            {
                agent.SetDestination(_targetPos);
            }
            else
            {
                poin++;
                if(poin >= targetPoint.GetPointsTarget().Length)
                {
                    poin = 0;
                }
            }
        }
    }
}
