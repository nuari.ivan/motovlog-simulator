using UnityEngine;

public class CameraSetting : MonoBehaviour
{
    [SerializeField] private Transform playerFollow;
    [SerializeField] private Transform bikeFollow;
    [SerializeField] private Transform playerLookAt;
    [SerializeField] private Transform bikeLookAt;
}
