using GaweDeweStudio;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BikeController : MonoBehaviour
{
    [SerializeField] private GameObject _biker;

    private MotorbikeController _controller;

    [SerializeField] private ButtonDrivingInteraction _gasButton;
    [SerializeField] private ButtonDrivingInteraction _brakeButton;
    [SerializeField] private ButtonDrivingInteraction _rightButton;
    [SerializeField] private ButtonDrivingInteraction _leftButton;

    [SerializeField] private GameObject[] allBikes;

    private void Start()
    {
        GameSetting.Instance.OnRiding += Instance_OnRiding;
        GameSetting.Instance.OnWalk += Instance_OnWalk;
        GameSetting.Instance.OnButtonHold += Instance_OnButtonHold;

        _controller = GetComponent<MotorbikeController>();

        SetupBikes(GameManager.Instance.activeSelectedBikeId);
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Sea"))
        {
            Scene s = SceneManager.GetActiveScene();
            GameManager.Instance.GetAds().RequestInterstitialAds();

            SceneManager.LoadSceneAsync(s.name);
        }
    }

    private void Instance_OnButtonHold(string id)
    {
        
    }

    private void OnDestroy()
    {
        GameSetting.Instance.OnRiding -= Instance_OnRiding;
        GameSetting.Instance.OnWalk -= Instance_OnWalk;
        GameSetting.Instance.OnButtonHold -= Instance_OnButtonHold;
    }

    private void Update()
    {
        if(_gasButton.gameObject.activeInHierarchy)
        {
            _controller.InputAtas(_gasButton.isPressed);
        }
        if (_brakeButton.gameObject.activeInHierarchy)
        {
            _controller.InputBawah(_brakeButton.isPressed);
        }
        if (_rightButton.gameObject.activeInHierarchy)
        {
            _controller.InputKanan(_rightButton.isPressed);
        }
        if (_leftButton.gameObject.activeInHierarchy)
        {
            _controller.InputKiri(_leftButton.isPressed);
        }
    }

    private void SetupBikes(int activeSelectedBikeId)
    {
        foreach (var item in allBikes)
        {
            item.SetActive(false);
        }

        allBikes[activeSelectedBikeId].SetActive(true);
    }

    private void Instance_OnRiding()
    {
        _controller.enabled = true;
        _controller.Reset();
        _biker.SetActive(true);
        GetComponent<Rigidbody>().isKinematic = false;
    }

    private void Instance_OnWalk()
    {
        _controller.enabled = false;
        _biker.SetActive(false);
        StartCoroutine(CheckOrang());
        GetComponent<Rigidbody>().isKinematic = true;
    }

    IEnumerator CheckOrang()
    {
        yield return new WaitForSeconds(1f);
        _biker.SetActive(false);
    }

    public void ResetBike()
    {
        _controller.Reset();
    }
}
