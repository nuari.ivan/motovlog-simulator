using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Item : MonoBehaviour
{
    [SerializeField] protected string itemName;
    [SerializeField] protected float itemValue;
    [SerializeField] protected float duration;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            TriggerAction(collision.gameObject);
        }
    }

    public abstract void TriggerAction(GameObject target);
}
