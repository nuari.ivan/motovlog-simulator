using System;
using UnityEngine;

namespace GaweDeweStudio
{
    public class GameManager : MonoBehaviour
    {
        public static GameManager Instance;

        [SerializeField] private PlayerData _data;

        [SerializeField] private GameState currentState;

        [SerializeField] private MotorDataSO activeBike;
        [SerializeField] private MotorDataSO[] allBikes;

        public int activeSelectedBikeId = 0;

        //private GameServices _services;
        private AdsManager _ads;
        private AudioManager _audio;

        public event Action<string> OnLogCreated;
        public event Action<int> OnBikeChanged;

        public delegate void StateChangeDelegate(GameState newState);
        public event StateChangeDelegate OnStateChanged;

        private void Awake()
        {
            if(Instance is null)
            {
                Instance = this;
            }
            else
            {
                Destroy(gameObject);
            }
            DontDestroyOnLoad(gameObject);

            //_services = GetComponent<GameServices>();
            _ads = GetComponentInChildren<AdsManager>();
            _audio = GetComponentInChildren<AudioManager>();

            CheckPlayerData();
        }

        private void CheckPlayerData()
        {
            if(PlayerPrefs.HasKey(Gamekey.PlayerName))
            {
                //load data
                PlayerData loadData = new PlayerData(PlayerPrefs.GetString(Gamekey.PlayerName), "new" , PlayerPrefs.GetInt(Gamekey.playerCoin));
                _data = loadData;
            }
            else
            {
                //create new player
                string newName = $"Player {UnityEngine.Random.Range(55, 999)}";
                
                PlayerPrefs.SetString(Gamekey.PlayerName, newName);
                PlayerPrefs.SetInt(Gamekey.playerCoin, 1000);
                _data = new PlayerData(newName, "new", 1000);
            }
        }

        private void _services_OnSignedIn(PlayerData newData)
        {
            _data = newData;
        }

        public void ChangeState(GameState newState)
        {
            if (newState == currentState)
            {
                return;
            }
            currentState = newState;

            OnStateChanged?.Invoke(currentState);
        }

        public PlayerData GetPlayerData()
        {
            return _data;
        }

/*        public GameServices GetServices()
        {
            return _services;
        }*/

        public AudioManager GetAudio()
        {
            return _audio;
        }

        public AdsManager GetAds()
        {
            return _ads;
        }

        public void CreateLog(string newLog)
        {
            OnLogCreated?.Invoke(newLog);
        }

        public void AddCoin(int value)
        {
            _data.coin += value;
            PlayerPrefs.SetInt(Gamekey.playerCoin, _data.coin);
        }

        public void ChangeName(string newName)
        {
            _data.username = newName;
            PlayerPrefs.SetString(Gamekey.PlayerName, _data.username);
        }

        public void ChangeBike(int newBike)
        {
            activeSelectedBikeId = newBike;
            activeBike = allBikes[newBike];
            OnBikeChanged?.Invoke(newBike);
        }

        public MotorDataSO GetCurrentBike()
        {
            return activeBike;
        }
    }


    [System.Serializable]
    public class PlayerData
    {
        public string username;
        public string userId;
        public int coin;

        public PlayerData(string name , string id, int coin)
        {
            username = name;
            userId = id;
            this.coin = coin;
        }
    }
}
