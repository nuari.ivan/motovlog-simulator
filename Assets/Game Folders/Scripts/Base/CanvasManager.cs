using System;
using UnityEngine;

namespace GaweDeweStudio 
{
    public class CanvasManager : MonoBehaviour
    {
        [SerializeField] protected Page[] allPages;
        [SerializeField] protected Widget[] allWidgets;

        protected virtual void Start()
        {
            allPages = GetComponentsInChildren<Page>(true);
            allWidgets = GetComponentsInChildren<Widget>(true);

            GameManager.Instance.OnStateChanged += Instance_OnStateChanged;

            GameManager.Instance.GetAds().InitializeAds();
        }
        private void OnDestroy()
        {
            GameManager.Instance.OnStateChanged -= Instance_OnStateChanged;
        }

        private void Instance_OnStateChanged(GameState newState)
        {
            switch (newState)
            {
                case GameState.Menu:
                    ChangePage(PageName.MainMenu);
                    break;
                case GameState.Setting:
                    ChangePage(PageName.Settings);
                    break;
                case GameState.Shop:
                    break;
                case GameState.ChangeBike:
                    ChangePage(PageName.ChangeBike);
                    break;
                case GameState.FreeCoin:
                    ChangePage(PageName.FreeCoin);
                    break;
                case GameState.ChangeName:
                    ChangePage(PageName.ChangeName);
                    break;
                case GameState.Opening:
                    break;
                case GameState.Game:
                    ChangePage(PageName.Game);
                    break;
                case GameState.Exit:
                    break;
                case GameState.Pause:
                    ChangePage(PageName.Pause);
                    break;
                case GameState.GameOver:
                    break;
                case GameState.Loading:
                    ChangePage(PageName.Loading);
                    break;
            }
        }

        protected void ChangePage(PageName newPage)
        {
            foreach (var p in allPages)
            {
                p.DisablePage();
            }

            Page findPage = Array.Find(allPages, p => p.pageName == newPage);
            if(findPage is null)
            {
                return;
            }
            findPage.gameObject.SetActive(true);
        }

        protected void ShowWidget(WidgetName newWidget)
        {
            Widget findWidget = Array.Find(allWidgets, w => w.namaWidget == newWidget);
            if(findWidget is null)
            {
                return;
            }
            findWidget.gameObject.SetActive(true);
        }
    }
}