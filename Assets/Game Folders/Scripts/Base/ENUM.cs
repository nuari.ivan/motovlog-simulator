public enum GameState
{
    Opening,
    Loading,
    Menu,
    Setting,
    Shop,
    ChangeBike,
    FreeCoin,
    Game,
    ChangeName,
    Exit,
    Pause,
    GameOver,
    Profile
}

public enum PageName
{
    Opening,
    MainMenu,
    Settings,
    Shop,
    ChangeBike,
    FreeCoin,
    Loading,
    ChangeName,
    Game,
    Pause,
    Result,
    Profile
}

public enum WidgetName
{
    Exit,
    Pause,
    NoConnection
}

public enum PlayModeState
{
    OnCharacter,
    OnBike
}

public class Constants
{
    public static string Coin = "coin";
    public static string Gem = "gem";
    public static string Exp = "exp";
}

public enum PLAYERSTATE
{
    IDLE,
    RUN,
    DIE,
    BATTLE_READY,
    HIT
}