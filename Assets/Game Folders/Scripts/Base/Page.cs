using UnityEngine;
using UnityEngine.UI;

namespace GaweDeweStudio
{
    public class Page : MonoBehaviour
    {
        public PageName pageName;

        protected virtual void Start()
        {
            Button[] allButtons = GetComponentsInChildren<Button>();
            foreach (var b in allButtons)
            {
                b.onClick.AddListener(() => GameManager.Instance.GetAudio().PlaySound("click"));
            }
        }

        public void DisablePage()
        {
            gameObject.SetActive(false);
        }
    }
}