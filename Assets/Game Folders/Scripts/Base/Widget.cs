using UnityEngine;
using UnityEngine.UI;

namespace GaweDeweStudio
{
    public class Widget : MonoBehaviour
    {
        public WidgetName namaWidget;

        protected virtual void Start()
        {
            Button[] allButtons = GetComponentsInChildren<Button>();
            foreach (var item in allButtons)
            {
                item.onClick.AddListener(() => GameManager.Instance.GetAudio().PlaySound("klik"));
            }
        }

        public void CloseWidget()
        {
            gameObject.SetActive(false);
        }
    }
}
