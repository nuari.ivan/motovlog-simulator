using GaweDeweStudio;
using UnityEngine;

public class MenuBikeSelector : MonoBehaviour
{
    [SerializeField] private GameObject[] allBikes;
    [SerializeField] private int indexSelected;

    private void Start()
    {
        GameManager.Instance.OnBikeChanged += Instance_OnBikeChanged;

        indexSelected = GameManager.Instance.activeSelectedBikeId;
        Instance_OnBikeChanged(indexSelected);
    }
    private void OnDestroy()
    {
        GameManager.Instance.OnBikeChanged -= Instance_OnBikeChanged;
    }

    private void Instance_OnBikeChanged(int currentIndex)
    {
        foreach (var item in allBikes)
        {
            item.SetActive(false);
        }

        allBikes[currentIndex].SetActive(true);
    }
}
