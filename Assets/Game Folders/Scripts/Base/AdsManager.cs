using GoogleMobileAds.Api;
using System;
using UnityEngine;

namespace GaweDeweStudio
{
    public class AdsManager : MonoBehaviour
    {
        [SerializeField] private string _bannerId;
        [SerializeField] private string _interstitialId;
        [SerializeField] private string _rewardedId;

        private BannerView _banner;
        private InterstitialAd _interstitial;
        private RewardedAd _rewarded;

        public event Action<bool> OnInterstitialSucceded;

        public void InitializeAds()
        {
            MobileAds.Initialize((InitializationStatus status) =>
            {

            });

            LoadBannerAds();
            LoadInterstitialAds();
            LoadRewardedAds();
        }

        private void LoadBannerAds()
        {
            if (_banner != null)
            {
                _banner.Destroy();
                _banner = null;
            }

            _banner = new BannerView(_bannerId, AdSize.Banner, AdPosition.Top);
            _banner.LoadAd(GetAdRequest());
        }

        private void LoadInterstitialAds()
        {
            if (_interstitial != null)
            {
                _interstitial.Destroy();
                _interstitial = null;
            }

            InterstitialAd.Load(_interstitialId, GetAdRequest(),
                (InterstitialAd ad, LoadAdError error) =>
                {
                    if (error != null || ad == null)
                    {
                        Debug.LogError("interstitial ad failed to load an ad " +
                                       "with error : " + error);
                        return;
                    }

                    Debug.Log("Interstitial ad loaded with response : "
                              + ad.GetResponseInfo());

                    _interstitial = ad;

                    SetInterstitialEvents();
                });
        }

        private void LoadRewardedAds()
        {
            if (_rewarded != null)
            {
                _rewarded.Destroy();
                _rewarded = null;
            }

            RewardedAd.Load(_rewardedId, GetAdRequest(),
                (RewardedAd ad, LoadAdError error) =>
                {
                    if (error != null || ad == null)
                    {
                        Debug.LogError("Rewarded ad failed to load an ad " +
                                       "with error : " + error);
                        return;
                    }

                    _rewarded = ad;

                    SetRewardedEvents();
                });
        }

        private AdRequest GetAdRequest()
        {
            var adRequest = new AdRequest();
            adRequest.Keywords.Add("my-admob-ads");

            return adRequest;
        }

        public void RequestBannerAds()
        {
            if (_banner == null)
            {
                LoadBannerAds();
                return;
            }

            _banner.Show();
        }

        public void RequestInterstitialAds()
        {
            if (_interstitial == null)
            {
                LoadInterstitialAds();
            }

            if (_interstitial.CanShowAd())
            {
                _interstitial.Show();
            }
        }

        public void RequestRewardedAds(string placement, Action<bool> OnSuccess)
        {
            if (_rewarded == null)
            {
                LoadRewardedAds();
            }

            if (_rewarded.CanShowAd())
            {
                _rewarded.Show((Reward reward) =>
                {
                    OnSuccess(true);
                });
            }
        }

        private void SetInterstitialEvents()
        {
            _interstitial.OnAdClicked += Interstitial_OnAdClicked;
            _interstitial.OnAdFullScreenContentClosed += Interstitial_OnAdFullScreenContentClosed;
            _interstitial.OnAdFullScreenContentFailed += Interstitial_OnAdFullScreenContentFailed;
            _interstitial.OnAdFullScreenContentOpened += Interstitial_OnAdFullScreenContentOpened;
            _interstitial.OnAdImpressionRecorded += Interstitial_OnAdImpressionRecorded;
            _interstitial.OnAdPaid += Interstitial_OnAdPaid;
        }

        private void SetRewardedEvents()
        {
            _rewarded.OnAdClicked += Rewarded_OnAdClicked;
            _rewarded.OnAdFullScreenContentClosed += Rewarded_OnAdFullScreenContentClosed;
            _rewarded.OnAdFullScreenContentFailed += Rewarded_OnAdFullScreenContentFailed;
            _rewarded.OnAdFullScreenContentOpened += Rewarded_OnAdFullScreenContentOpened;
            _rewarded.OnAdImpressionRecorded += Rewarded_OnAdImpressionRecorded;
            _rewarded.OnAdPaid += Rewarded_OnAdPaid;
        }

        #region EVENT CALLBACKS
        private void Interstitial_OnAdClicked()
        {
        }

        private void Interstitial_OnAdFullScreenContentClosed()
        {
            OnInterstitialSucceded?.Invoke(true);
            LoadInterstitialAds();
        }

        private void Interstitial_OnAdFullScreenContentFailed(AdError obj)
        {
            OnInterstitialSucceded?.Invoke(false);
            LoadInterstitialAds();
        }

        private void Interstitial_OnAdFullScreenContentOpened()
        {
        }

        private void Interstitial_OnAdImpressionRecorded()
        {
        }

        private void Interstitial_OnAdPaid(AdValue obj)
        {
        }

        private void Rewarded_OnAdClicked()
        {
        }

        private void Rewarded_OnAdFullScreenContentClosed()
        {
            LoadRewardedAds();
        }

        private void Rewarded_OnAdFullScreenContentFailed(AdError obj)
        {
            LoadRewardedAds();
        }

        private void Rewarded_OnAdFullScreenContentOpened()
        {
        }

        private void Rewarded_OnAdImpressionRecorded()
        {
        }

        private void Rewarded_OnAdPaid(AdValue obj)
        {
        }
        #endregion
    }
}
