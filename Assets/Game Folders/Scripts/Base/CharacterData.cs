using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Character" , menuName = "New/Character")]
public class CharacterData : ScriptableObject
{
    public string characterName;

    [Header("Base Stats")]
    public float characterSpeed;
    public float characterJumpForce;

    [Header("Battle Stats")]
    public float health;

    public float atkSpeed;
    public int atk;
    public int def;
}
