using System;
using UnityEngine;

namespace GaweDeweStudio
{
    public class AudioManager : MonoBehaviour
    {
        [SerializeField] private Sound[] allSounds;

        private void Awake()
        {
            GenerateSounds();
        }

        private void GenerateSounds()
        {
            foreach (Sound s in allSounds)
            {
                s.source = gameObject.AddComponent<AudioSource>();
                s.source.clip = s.klip;
                s.source.volume = s.volume;
                s.source.loop = s.isLooping;
            }
        }

        public void PlaySound(string soundName)
        {
            Sound findSound = Array.Find(allSounds, s => s.namaSound == soundName);
            if(findSound != null)
            {
                if(findSound.tipe == SoundType.BGM)
                {
                    foreach (Sound item in GetBgmSounds())
                    {
                        item.source.Stop();
                    }
                }
                findSound.source.Play();
            }
        }

        private Sound[] GetBgmSounds()
        {
            Sound[] temp = Array.FindAll(allSounds, s => s.tipe == SoundType.BGM);
            return temp;
        }

        public void SetToMute(bool isMute)
        {
            foreach (var item in allSounds)
            {
                item.source.mute = isMute;
            }
        }
    }

    

    public enum SoundType
    {
        BGM,
        SFX
    }


    [System.Serializable]
    public class Sound
    {
        public string namaSound;
        public SoundType tipe = SoundType.SFX;
        public AudioClip klip;
        [Range(0f , 100f)] public float volume;
        public bool isLooping;

        [HideInInspector]
        public AudioSource source;
    }
}