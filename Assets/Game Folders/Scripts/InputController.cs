using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Cinemachine;

[RequireComponent(typeof(SwipeManager))]
public class InputController : MonoBehaviour
{
    public CinemachineFreeLook _cinemachine;

    void Start()
    {
        SwipeManager swipeManager = GetComponent<SwipeManager>();
        
        swipeManager.onSwipe += HandleSwipe;
        swipeManager.onLongPress += HandleLongPress;
        swipeManager.onDragSwipe += SwipeManager_onDragSwipe;
    }

    private void SwipeManager_onDragSwipe(SwipeAction swipeAction)
    {
        float xInput = 0;
        float yInput = 0;

        //Debug.LogFormat("HandleSwipe: {0}", swipeAction);
        if (swipeAction.direction == SwipeDirection.Up || swipeAction.direction == SwipeDirection.UpRight)
        {
            // move up
            yInput = 1f;
        }
        else if (swipeAction.direction == SwipeDirection.Right || swipeAction.direction == SwipeDirection.DownRight)
        {
            // move right
            xInput = 1f;
        }
        else if (swipeAction.direction == SwipeDirection.Down || swipeAction.direction == SwipeDirection.DownLeft)
        {
            // move down
            yInput = -1f;
        }
        else if (swipeAction.direction == SwipeDirection.Left || swipeAction.direction == SwipeDirection.UpLeft)
        {
            // move left
            xInput = -1f;
        }

        _cinemachine.m_XAxis.Value += xInput * 200 * Time.deltaTime;
        _cinemachine.m_YAxis.Value += -yInput * 50 * Time.deltaTime;
    }

    void HandleSwipe(SwipeAction swipeAction)
    {
        
    }

    void HandleLongPress(SwipeAction swipeAction)
    {
        
    }
}