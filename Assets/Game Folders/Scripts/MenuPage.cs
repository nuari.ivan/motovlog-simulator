using GaweDeweStudio;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuPage : Page
{
    [SerializeField] private Button playButton;
    [SerializeField] private Button settingButton;
    [SerializeField] private Button selectBikeButton;
    [SerializeField] private Button freeAdsButton;
    [SerializeField] private Button changeNameButton;

    [SerializeField] private TMP_Text labelNameText;
    [SerializeField] private TMP_Text labelMoneyText;

    protected override void Start()
    {
        base.Start();

        playButton.onClick.AddListener(() =>
        {
            GameManager.Instance.GetAds().RequestInterstitialAds();
            GameManager.Instance.ChangeState(GameState.Loading);
        });

        selectBikeButton.onClick.AddListener(() => GameManager.Instance.ChangeState(GameState.ChangeBike));
        settingButton.onClick.AddListener(() => GameManager.Instance.ChangeState(GameState.Setting));

        freeAdsButton.onClick.AddListener(() => GameManager.Instance.ChangeState(GameState.FreeCoin));
        changeNameButton.onClick.AddListener(() => GameManager.Instance.ChangeState(GameState.ChangeName));
    }

    private void OnEnable()
    {
        labelNameText.text = GameManager.Instance.GetPlayerData().username;
        labelMoneyText.text = $"Rp. {GameManager.Instance.GetPlayerData().coin} ,00";
    }
}
