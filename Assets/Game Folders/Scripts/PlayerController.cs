using StarterAssets;
using System.Collections;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [Header("PLAYER AREA")]
    [SerializeField] private LayerMask bikeMask;
    [SerializeField] private float rangeArea;

    [SerializeField] private ButtonDrivingInteraction sprintButton;

    [SerializeField] private float _camSpeed = 10f;

    [SerializeField] private bool isBikeAround = false;
    private bool _onBike = false;

    [SerializeField] private GameObject _character;

    private Animator _anim;
    private ThirdPersonController _controller;
    private CharacterController _motor;
    private StarterAssetsInputs _input;

    private void Start()
    {
        GameSetting.Instance.OnRiding += Instance_OnRiding;
        GameSetting.Instance.OnWalk += Instance_OnWalk;
        GameSetting.Instance.OnCharacterInput += Instance_OnCharacterInput;
        GameSetting.Instance.OnCameraInput += Instance_OnCameraInput;
        GameSetting.Instance.OnPlayerJump += Instance_OnPlayerJump;

        _controller = GetComponent<ThirdPersonController>();
        _motor = GetComponent<CharacterController>();
        _anim = GetComponent<Animator>();
        _input = GetComponent<StarterAssetsInputs>();

    }

    private void OnDestroy()
    {
        GameSetting.Instance.OnRiding -= Instance_OnRiding; 
        GameSetting.Instance.OnWalk -= Instance_OnWalk;
        GameSetting.Instance.OnCharacterInput -= Instance_OnCharacterInput; 
        GameSetting.Instance.OnCameraInput -= Instance_OnCameraInput;
        GameSetting.Instance.OnPlayerJump -= Instance_OnPlayerJump;
    }

    private void Instance_OnPlayerJump(bool isJump)
    {
        _input.jump = isJump;
    }

    private void Instance_OnCharacterInput(float _horizontal, float _vertical)
    {
        if(_motor.enabled)
        {
            _input.move.x = _horizontal;
            _input.move.y = _vertical;
        }
    }

    private void Instance_OnCameraInput(float horizontal, float vertical)
    {
        if(_input.enabled)
        {
            _input.look.x = horizontal * _camSpeed;
            _input.look.y = -vertical * _camSpeed;
        }
    }

    private void Update()
    {
        if (_onBike)
        {
            transform.position = GameSetting.Instance.GetBike().transform.position;
        }

        if(sprintButton.gameObject.activeInHierarchy)
        {
            _input.SprintInput(sprintButton.isPressed);
        }
    }

    private void Instance_OnRiding()
    {
        _character.SetActive(false);
        SetToBike(true);
    }

    private void Instance_OnWalk()
    {
        SetToBike(false);
        _onBike = false;

        StartCoroutine(TurunMotor());
    }

    IEnumerator TurunMotor()
    {
        yield return new WaitForSeconds(0.1f);
        Vector3 _newPos = transform.position;
        _newPos.x += 2.5f;

        transform.position = _newPos;
        _character.SetActive(true);
    }

    private void FixedUpdate()
    {
        isBikeAround = Physics.CheckSphere(transform.position, rangeArea, bikeMask);
        if(GameSetting.Instance != null)
        {
            GameSetting.Instance.PlayerNearofBike(isBikeAround);
        }
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.DrawWireSphere(transform.position, rangeArea);
    }

    public void SetToBike(bool onBike)
    {
        _motor.enabled = !onBike;
        _onBike = onBike;
        
    }
}
