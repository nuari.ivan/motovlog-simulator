using GaweDeweStudio;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ChangeNamePage : Page
{
    [SerializeField] private Button yesButton;
    [SerializeField] private Button backButton;

    [SerializeField] private TMP_InputField inputNewNameInputfield;

    [SerializeField] private GameObject panelCongrats;
    [SerializeField] private Button closeButton;

    protected override void Start()
    {
        base.Start();
        panelCongrats.SetActive(false);

        backButton.onClick.AddListener(() => GameManager.Instance.ChangeState(GameState.Menu));
        yesButton.onClick.AddListener(() =>
        {
            if (string.IsNullOrEmpty(inputNewNameInputfield.text))
            {
                return;
            }

            GameManager.Instance.GetAds().RequestRewardedAds(Gamekey.ChangeName, (isDone) =>
            {
                GameManager.Instance.ChangeName(inputNewNameInputfield.text);
                panelCongrats.SetActive(true);
                yesButton.interactable = false;
            });
        });

        closeButton.onClick.AddListener(() => panelCongrats.SetActive(false));
    }
}
