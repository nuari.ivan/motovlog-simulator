using GaweDeweStudio;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PausePage : Page
{
    [SerializeField] private Button yesButton;
    [SerializeField] private Button closeButton;

    protected override void Start()
    {
        base.Start();
        yesButton.onClick.AddListener(() =>
        {
            GameManager.Instance.GetAds().RequestInterstitialAds();
            SceneManager.LoadSceneAsync(1, LoadSceneMode.Single);
        });

        closeButton.onClick.AddListener(() => GameManager.Instance.ChangeState(GameState.Game));
    }
}
