using UnityEngine;

public class TargetPointPedestrianGroup : MonoBehaviour
{
    [SerializeField] private Transform[] points;


    public Transform[] GetPointsTarget()
    {
        return points;
    }
}
