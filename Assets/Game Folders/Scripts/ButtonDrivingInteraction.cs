using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ButtonDrivingInteraction : MonoBehaviour , IPointerDownHandler, IPointerUpHandler
{
    public string id;
    public bool isPressed;

    [SerializeField] private Color normalColor;
    [SerializeField] private Color holdColor;


    private Image gambar;

    private void Start()
    {
        gambar = GetComponent<Image>();
    }

    public void OnHold()
    {
        
    }

    public void OnReleaseHold()
    {
       
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        gambar.color = holdColor;
        isPressed = true;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        gambar.color = normalColor;
        isPressed = false;
    }
}
