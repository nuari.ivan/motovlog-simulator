using Cinemachine;
using System;
using System.Collections;
using UnityEngine;

public class GameSetting : MonoBehaviour
{
    public static GameSetting Instance;

    [SerializeField] private PlayModeState currentState;

    [SerializeField] private PlayerController _player;
    [SerializeField] private BikeController _bike;

    [SerializeField] private CinemachineVirtualCamera[] motorCamera;
    private int activeCam;

    [SerializeField] private int maxPlayTime = 60;
    private int playTime;

    public event Action<bool> OnNearBike;
    public event Action OnRiding;
    public event Action OnWalk;
    public event Action<string> OnButtonHold;

    public event Action<float, float> OnCharacterInput;
    public event Action<float, float> OnCameraInput;
    public event Action<int> OnTimerUpdate;
    public event Action<bool> OnPlayerJump;

    private void Awake()
    {
        Time.timeScale = 1f;
        Application.targetFrameRate = 60;

        if(Instance == null)
        {
            Instance = this;
        }

        StartCoroutine(StartTimer());
    }

    IEnumerator StartTimer()
    {
        for (int i = 0; i < maxPlayTime; i++)
        {
            OnTimerUpdate?.Invoke(playTime);
            yield return new WaitForSeconds(1.5f);
            playTime++;
        }
    }

    public void PlayerNearofBike(bool isNear)
    {
        OnNearBike?.Invoke(isNear);
    }

    public void PlayerJump(bool isJump)
    {
        OnPlayerJump?.Invoke(isJump);
    }

    public void ChangeState(PlayModeState state)
    {
        if(state == currentState)
        {
            return;
        }
        currentState = state;

        switch (state)
        {
            case PlayModeState.OnCharacter:
                motorCamera[activeCam].Priority = 0;
                OnWalk?.Invoke();
                break;
            case PlayModeState.OnBike:
                activeCam = 0;
                motorCamera[activeCam].Priority = 15;
                OnRiding?.Invoke();
                break;
        }
    }

    public void ChangeCam()
    {
        activeCam++;
        if (activeCam >= motorCamera.Length)
        {
            activeCam = 0;
        }
        foreach (var item in motorCamera)
        {
            item.Priority = 0;
        }
        motorCamera[activeCam].Priority = 15;
    }

    public void GetCharacterWalk()
    {
        currentState = PlayModeState.OnCharacter;
        OnWalk?.Invoke();
    }

    public BikeController GetBike()
    {
        return _bike;
    }

    public PlayerController GetPLayer()
    {
        return _player;
    }

    public PlayModeState GetCurrentState()
    {
        return currentState;
    }

    public void PressButton(string id)
    {
        OnButtonHold?.Invoke(id);
    }

    public void SetInput(float horizontal, float vertical)
    {
        OnCharacterInput?.Invoke(horizontal, vertical);
    }

    public void SetCameraInput(float horizontal, float vertical)
    {
        OnCameraInput?.Invoke(horizontal, vertical);
    }
}
