using UnityEngine;

[CreateAssetMenu(fileName = "New Bike" , menuName = "New/Bike")]
public class MotorDataSO : ScriptableObject
{
    public string namaMotor;
    public int idIndex;
    public GameObject prefab;
    public int harga;
    public bool terbeli;

}
