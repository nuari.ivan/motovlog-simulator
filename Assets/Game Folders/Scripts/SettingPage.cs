using GaweDeweStudio;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class SettingPage : Page
{
    [SerializeField] private Button backButton;

    [SerializeField] private Toggle audioToggle;
    [SerializeField] private TMP_Dropdown graphicDropdown;

    protected override void Start()
    {
        base.Start();

        backButton.onClick.AddListener(() => GameManager.Instance.ChangeState(GameState.Menu));
        audioToggle.onValueChanged.AddListener((isOn) =>
        {
            GameManager.Instance.GetAudio().SetToMute(!isOn);
        });
        graphicDropdown.onValueChanged.AddListener((value) =>
        {

        });
    }
}