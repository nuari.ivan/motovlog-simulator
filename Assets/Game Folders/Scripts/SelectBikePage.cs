using GaweDeweStudio;
using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class SelectBikePage : Page
{
    [SerializeField] private Button backButton;

    [SerializeField] private Button leftButton;
    [SerializeField] private Button rightButton;

    [SerializeField] private TMP_Text labelBikeText;

    [SerializeField] private TMP_Text labelNameText;
    [SerializeField] private TMP_Text labelMoneyText;

    private int activeBike;

    protected override void Start()
    {
        base.Start();
        activeBike = GameManager.Instance.activeSelectedBikeId;

        backButton.onClick.AddListener(() => GameManager.Instance.ChangeState(GameState.Menu));
        leftButton.onClick.AddListener(() => ChangeBikeSelected(false));
        rightButton.onClick.AddListener(() => ChangeBikeSelected(true));

        MotorDataSO activeBikeData = GameManager.Instance.GetCurrentBike();
        labelBikeText.text = $"{activeBikeData.namaMotor}";
    }

    private void ChangeBikeSelected(bool isRight)
    {
        if(isRight)
        {
            activeBike++;
            if(activeBike >= 5)
            {
                activeBike = 4;
            }
        }
        else
        {
            activeBike--;
            if(activeBike <= 0)
            {
                activeBike = 0;
            }
        }

        GameManager.Instance.ChangeBike(activeBike);

        MotorDataSO activeBikeData = GameManager.Instance.GetCurrentBike();
        labelBikeText.text = $"{activeBikeData.namaMotor}";
    }

    private void OnEnable()
    {
        labelNameText.text = GameManager.Instance.GetPlayerData().username;
        labelMoneyText.text = $"Rp. {GameManager.Instance.GetPlayerData().coin} ,00";
    }
}
