using GaweDeweStudio;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class InitializePage : Page
{
    [SerializeField] private Slider loadingSlider;
    [SerializeField] private TMP_Text loadingText;

    protected override void Start()
    {
        base.Start();
        StartCoroutine(LoadingToPlay());
    }

    IEnumerator LoadingToPlay()
    {
        while (loadingSlider.value < loadingSlider.maxValue)
        {
            yield return new WaitForSeconds(0.1f);
            loadingSlider.value += 6f;
            loadingText.text = $"Loading . . .  {loadingSlider.value} %";
        }

        SceneManager.LoadSceneAsync(1, LoadSceneMode.Single);
    }
}
